#!/usr/bin/ruby

require 'nickel'
require 'json'

inputJS = JSON.parse(STDIN.gets().chomp)

module Nickel
    class ZDate
        def toTW
            year_str + '-' + month_str + '-' + day_str
        end
    end
end


if (inputJS.has_key?('due'))
    puts(inputJS.to_json)
else
    nlp = Nickel.parse(inputJS['description'])
    if (nlp.occurrences == []) 
        puts inputJS.to_json
    else
        if (inputJS.has_key?('annotations')) 
            inputJS['annotations'].push({"entry" => Time.now.iso8601, "description" => "NLP unparsed: " + inputJS['description']})
        else
            inputJS['annotations'] = [{"entry" => Time.now.iso8601, "description" => "NLP unparsed: " + inputJS['description']}]
        end
        inputJS['description'] = nlp.message
        due_str = nlp.occurrences[0].start_date.toTW
        if (nlp.occurrences[0].start_time)
            due_str += "T#{nlp.occurrences[0].start_time.readable}"
        end
        inputJS['due'] = due_str
        puts inputJS.to_json
        puts "NLP: Due date detected and set to: #{inputJS['due']}"
    end
end
