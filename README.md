# Natural language parsing for tasks

Simple hooks that parse tasks added to taskwarrior and will update/edit the task based on the result

## Time parsing

`on-add.nlptime.rb` parses your task and will set a due date if a date is found in the description. For example, `task add Buy groceries 2pm today` will add a “Buy groceries” task due for today at 14:00. The original description is added as an annotation.

The hook is written in ruby and relies on the [Nickel](https://github.com/iainbeeston/nickel) ruby library for the actual task parsing. As such you need to install ruby and then install the Nickel library with `gem install nickel`.

To install the hook just copy it to `.task/hooks/` and make it executable.


